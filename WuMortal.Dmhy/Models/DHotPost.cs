﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WuMortal.Dmhy.Models
{
    public class DHotPost
    {
        public string Id { get; set; }

        public string ImgUrl { get; set; }

        public string Name { get; set; }
    }
}
