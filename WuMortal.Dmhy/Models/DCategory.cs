﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WuMortal.Dmhy.Models
{
    public class DCategory
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
